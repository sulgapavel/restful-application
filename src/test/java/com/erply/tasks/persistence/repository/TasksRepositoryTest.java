package com.erply.tasks.persistence.repository;

import com.erply.tasks.persistence.entity.Task;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DataJpaTest
class TasksRepositoryTest {

    @Inject
    private TasksJpaRepository tasksJpaRepository;

    @Test
    void save() {

        final Task expected = new Task();
        expected.setDescription(UUID.randomUUID().toString());
        tasksJpaRepository.save(expected);

        final List<Task> actualTasks = tasksJpaRepository.findByDescription(expected.getDescription());

        assertEquals(1, actualTasks.size());
        assertEquals(expected.getDescription(), actualTasks.get(0).getDescription());

    }
}