package com.erply.tasks.api;

import com.erply.tasks.persistence.entity.Task;
import com.erply.tasks.persistence.repository.TasksJpaRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class TasksControllerTest {


    @Inject
    private MockMvc mvc;

    @Inject
    private ObjectMapper objectMapper;

    @Inject
    private TasksJpaRepository tasksRepository;

    @Test
    void getTasks() throws Exception {

        final Task expectedTask = Task.builder().description(UUID.randomUUID().toString()).build();
        tasksRepository.save(expectedTask);

        final ResultActions resultActions = this.mvc.perform(get("/api/tasks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));


        final String json = resultActions.andReturn().getResponse().getContentAsString();

        final List<Task> allActualTasks = objectMapper.readValue(json, new TypeReference<List<Task>>() {
        });

        final List<Task> actualTasks = allActualTasks.stream().filter(t -> t.getDescription().equals(expectedTask.getDescription())).collect(Collectors.toList());

        assertEquals(1, actualTasks.size());

    }

    @Test
    void getTasksById() throws Exception {

        final Task expectedTask = Task.builder().description(UUID.randomUUID().toString()).build();
        tasksRepository.save(expectedTask);

        final ResultActions perform = mvc.perform(get("/api/tasks/{id}", expectedTask.getId())
                .contentType("application/json"))
                .andExpect(status().isOk());


        final String json = perform.andReturn().getResponse().getContentAsString();

        final Task actualTask = objectMapper.readValue(json, Task.class);

        assertEquals(expectedTask, actualTask);

    }

    @Test
    void createTask() throws Exception {

        final Task task = Task.builder().description(UUID.randomUUID().toString()).build();


        final ResultActions perform = mvc.perform(post("/api/tasks")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(task)));

        final String location = perform.andReturn().getResponse().getHeader("Location");
        assertTrue(location.startsWith("http://localhost/api/tasks"));


        perform.andExpect(status().isCreated());


        final Long actualId = Long.valueOf(location.substring(location.lastIndexOf('/') + 1));
        final Task actualTask = tasksRepository.findById(actualId).get();

        assertEquals(task.getDescription(), actualTask.getDescription());

    }

    @Test
    void updateTask() throws Exception {
        final Task task = Task.builder().description("Take a rest buddy, 5 commits in 2 hours...").build();
        tasksRepository.save(task);

        task.setDescription("new description");

        final ResultActions perform = mvc.perform(put("/api/tasks/{id}", task.getId())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(task)));

        perform.andExpect(status().isCreated());

        final String location = perform.andReturn().getResponse().getHeader("Location");
        final Long actualId = Long.valueOf(location.substring(location.lastIndexOf('/') + 1));

        final Task actualTask = tasksRepository.findById(actualId).get();

        assertEquals("new description", actualTask.getDescription());

    }

    @Test
    void deleteTask() throws Exception {

        final Task task = Task.builder().description("Take a rest buddy, 5 commits in 2 hours...").build();
        tasksRepository.save(task);

        final ResultActions perform = mvc.perform(delete("/api/tasks/{id}", task.getId()));

        perform.andExpect(status().isNoContent());

        final Optional<Task> byId = tasksRepository.findById(task.getId());

        assertTrue(byId.isEmpty());

    }


    @Test
    void setCompleted() throws Exception {

        final Task task = Task.builder().description("Take a rest buddy, 5 commits in 2 hours...").build();
        tasksRepository.save(task);
        assertFalse(task.isCompleted());

        final ResultActions perform = mvc.perform(patch("/api/tasks/{id}", task.getId()));

        perform.andExpect(status().isOk());


        final Task actualTask = tasksRepository.findById(task.getId()).get();
        assertTrue(actualTask.isCompleted());


    }
}
