package com.erply.tasks.persistence.repository;

import com.erply.tasks.persistence.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TasksJpaRepository extends JpaRepository<Task, Long> {

    List<Task> findByDescription(String description);
}
