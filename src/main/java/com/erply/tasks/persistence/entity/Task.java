package com.erply.tasks.persistence.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;


@AllArgsConstructor
@Setter
@Getter
@Entity
@Builder
@EqualsAndHashCode
public class Task {

    @Id
    @GeneratedValue
    private Long id;

    private String description;

    private LocalDateTime created;
    private LocalDateTime modified;
    private boolean completed;

    public Task() {
        LocalDateTime date = LocalDateTime.now();
        this.created = date;
        this.modified = date;
    }

}
