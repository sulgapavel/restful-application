package com.erply.tasks.api;


import com.erply.tasks.persistence.entity.Task;
import com.erply.tasks.persistence.repository.TasksJpaRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@Api(value="Tasks Management System", description="Operations regarding tasks CRUD actions")
public class TasksController {

    @Inject
    private TasksJpaRepository repository;

    @ApiOperation(value = "View a list of all tasks, that exist in database", response = ResponseEntity.class)
    @GetMapping("/tasks")
    public ResponseEntity<Iterable<Task>> getTasks() {
        return ResponseEntity.ok(repository.findAll());
    }

    @ApiOperation(value = "Find a certain task by task ID", response = ResponseEntity.class)
    @GetMapping("/tasks/{id}")
    public ResponseEntity<Task> getTasksById(@PathVariable Long id) {

        return ResponseEntity.ok(repository.findById(id).get());

    }

    @ApiOperation(value = "Update certain task by its ID", response = ResponseEntity.class)
    @PutMapping("/tasks/{id}")
    public ResponseEntity<Task> updateTask(@Valid @RequestBody Task task) {
        Task result = repository.save(task);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Create a task, using RequestBody parameters", response = ResponseEntity.class)
    @PostMapping("/tasks")
    public ResponseEntity<Task> createTask(@Valid @RequestBody Task task) {
        final Task result = repository.save(task);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                path("/{id}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @ApiOperation(value = "Delete a task by its ID", response = ResponseEntity.class)
    @DeleteMapping("/tasks/{id}")
    public ResponseEntity<Task> deleteTask(@PathVariable Long id) {
        repository.delete(Task.builder().id(id).build());
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Delete all of the tasks, that exist in database", response = ResponseEntity.class)
    @DeleteMapping("/tasks")
    public ResponseEntity<Task> deleteTasks(Task task) {
        repository.delete(task);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Set task complete level to be true", response = ResponseEntity.class)
    @PatchMapping("/tasks/{id}")
    public ResponseEntity<Task> setCompleted(final @PathVariable Long id) {
        final Optional<Task> byId = repository.findById(id);

        if (byId.isPresent()) {
            final Task result = byId.get();
            result.setCompleted(true);
            repository.save(result);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand(result.getId()).toUri();
            return ResponseEntity.ok().header("Location", location.toString()).build();
        } else {
            return ResponseEntity.notFound().build();
        }

    }

}
