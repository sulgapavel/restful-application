---------------MODULES------------------------

Spring Boot
Spring MVC
Hibernate
H2 In-memory database
JUnit
Lombok
JDBC
Swagger
Tomcat
Mockito (Tests for repository and controller included)
Gradle

-----------------URLs-------------------------

localhost:8080 by default

/api/tasks - [GET] - Returns a list of all tasks
/api/tasks/{id} - [GET] - Returns a specific task by ID
/api/tasks/{id} - [PUT] - Updates a specific task by ID
/api/tasks - [POST] - Creates a new task and adds it to the list
/api/tasks/{id} - [DELETE] - Deletes a certain task by ID
/api/tasks - [DELETE] - Deletes all tasks
/api/tasks/{id} - [PATCH] - Sets task to be Completed by ID



-------------------EXECUTABLE------------------

build executable jar: ./gradlew build
run application: java -jar build/libs/tasks.jar